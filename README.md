Setup first user and sshdconfig
=========

This role sets up a brand new server with a new user (that's not root) and configures `sshd_config`.
The new user will gain remote ssh access via publickey authentication and lose password authentication.
Ssh root access is denied.
The ssh config can be found in the `templates/` directory.

Run this role in a separate playbook prior to the main provisioning on CentOS 8/RHEL 8 systems.

The role does the following:
  1. sets up the first user and adds them to the `wheel` group
  2. adds the ssh `authorized_keys` file for the first user
  3. installs `semanage` to manage SELinux ports
  4. allows the new ssh port in SELinux
  5. opens the new ssh port in firewalld
  6. templates `sshd_config` with the new ssh port
  7. notifies the handlers to restart the sshd and firewalld services

Warning: If `sshport` variable is set, this role will change the default ssh port from 22 to something else.

`sshd_config` can be changed by changing the template file in `templates/sshd_config.j2`.

After this role is run, subsequent playbooks will need `ansible_ssh_port` and `ansible_user` changed:

    ansible-playbook -e "ansible_user=<first user>" -e "ansible_ssh_port=<new sshport>" -K -i inventory setup-firstuser-and-template-sshd.yml 

Replace `<first user>` with the new first user and `<new sshport>` with the new ssh port.

Requirements
------------

1. Ssh root access to server

Role Variables
--------------

Available variables are listed below. `firstuser`, `firstuserpw`, and `pubkey` are required. `sshport` defaults to 22.

    firstuser
Set `firstuser` to be the username of the first user. It will get sudo access.

    firstuserpw
Set `firstuserpw` to the password of the first user. 
See this [FAQ](https://docs.ansible.com/ansible/latest/reference_appendices/faq.html#how-do-i-generate-encrypted-passwords-for-the-user-module) to encrypt this variable.

    pubkey
Your public key.

    sshport
Set `sshport` to the desired ssh port.
To choose a new ssh port, see this [list of port numbers used by the Internet protocol suite. Choose a new ssh port that's not used by a protocol you're using.](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers)

```
timezone (default: America/New_York)
```
Sets the time zone of the machine.

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: servers
      become: yes
      vars:
        firstuser: <username>
        firstuserpw: <encrypted_pass_var>
        pubkey: <ssh_public_key>
        sshport: <some_other_port>
      roles:
         - name: setup-ssh-and-firstuser

After denying ssh access to root and changing the ssh port, you can re-run this role by setting `ansible_user` to the newly created user and setting `ansible_ssh_port` to the new port.
This makes the role idempotent:

    - hosts: servers
      become: yes
      vars:
        ansible_user: <username>
        ansible_ssh_port: <some_other_port>
        firstuser: <username>
        firstuserpw: <encrypted_pass_var>
        pubkey: <ssh_public_key>
        sshport: <some_other_port>
      roles:
         - name: setup-ssh-and-firstuser

License
-------

BSD

Author Information
------------------

May 2020 - G. F. AbuAkel
